unit MainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, frxClass,
  frxDBSet;

type
  TFormMain = class(TForm)
    Con: TADOConnection;
    QryMain: TADOQuery;
    QryMainTransID: TIntegerField;
    QryMainBillNo: TStringField;
    QryMainBillDate: TDateTimeField;
    QryMainItemID: TIntegerField;
    QryMainItemName: TStringField;
    QryMainQty: TFloatField;
    QryMainRate: TFloatField;
    QryMainAmount: TBCDField;
    frxReport1: TfrxReport;
    frxDBMain: TfrxDBDataset;
    QryMainNar1: TStringField;
    QryMainNar2: TStringField;
    QryMainNar3: TStringField;
    QryMainNar4: TStringField;
    QryMainNar5: TStringField;
    frxReport2: TfrxReport;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

{$R *.dfm}

end.
