object FormMain: TFormMain
  Left = 0
  Top = 0
  Caption = 'FormMain'
  ClientHeight = 425
  ClientWidth = 663
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Con: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=SaAdmin@123;Persist Security Info=T' +
      'rue;User ID=Sa;Initial Catalog=DURGA2122;Data Source=BHAVESH\SQL' +
      '2012'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 56
    Top = 64
  end
  object QryMain: TADOQuery
    Active = True
    Connection = Con
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'SELECT T.TransID,'
      #9'   T.BillNo,'
      #9'   T.BillDate,'
      #9'   TD.ItemID,'
      #9'   Item.ItemName,'
      '     IsNull(TD.Nar1,'#39#39')Nar1,'
      '    IsNull(TD.Nar2,'#39#39')Nar2,'
      '    IsNull(TD.Nar3,'#39#39')Nar3,'
      '    IsNull(TD.Nar4,'#39#39')Nar4,'
      '    IsNull(TD.Nar5,'#39#39')Nar5,'
      #9'   TD.Qty,'
      #9'   TD.Rate,'
      #9'   TD.Amount'
      'FROM Trans T'
      'JOIN TransDetail TD ON TD.TransID = T.TransID'
      'JOIN ITem ON item.ItemID = TD.ItemID')
    Left = 56
    Top = 128
    object QryMainTransID: TIntegerField
      FieldName = 'TransID'
      ReadOnly = True
    end
    object QryMainBillNo: TStringField
      FieldName = 'BillNo'
      FixedChar = True
    end
    object QryMainBillDate: TDateTimeField
      FieldName = 'BillDate'
    end
    object QryMainItemID: TIntegerField
      FieldName = 'ItemID'
    end
    object QryMainItemName: TStringField
      FieldName = 'ItemName'
      Size = 50
    end
    object QryMainQty: TFloatField
      FieldName = 'Qty'
    end
    object QryMainRate: TFloatField
      FieldName = 'Rate'
    end
    object QryMainAmount: TBCDField
      FieldName = 'Amount'
      Precision = 19
    end
    object QryMainNar1: TStringField
      FieldName = 'Nar1'
      Size = 50
    end
    object QryMainNar2: TStringField
      FieldName = 'Nar2'
      Size = 50
    end
    object QryMainNar3: TStringField
      FieldName = 'Nar3'
      Size = 50
    end
    object QryMainNar4: TStringField
      FieldName = 'Nar4'
      Size = 50
    end
    object QryMainNar5: TStringField
      FieldName = 'Nar5'
      Size = 50
    end
  end
  object frxReport1: TfrxReport
    Version = '6.4'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44459.658588449100000000
    ReportOptions.LastChange = 44459.674140219900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.          ')
    Left = 288
    Top = 64
    Datasets = <
      item
        DataSet = frxDBMain
        DataSetName = 'frxDBMain'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Visible = False
      PaperWidth = 297.000000000000000000
      PaperHeight = 420.000000000000000000
      PaperSize = 8
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Height = 41.574830000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        DataSet = frxDBMain
        DataSetName = 'frxDBMain'
        RowCount = 0
        Stretched = True
        object frxDBMainBillNo: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 1.000000000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'BillNo'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."BillNo"]')
        end
        object frxDBMainBillDate: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 1.000000000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'BillDate'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."BillDate"]')
        end
        object frxDBMainItemName: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 23.677143390000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."ItemName"]'
            
              '[frxDBMain."Nar1"] [frxDBMain."Nar2"] [frxDBMain."Nar3"] [frxDBM' +
              'ain."Nar4"]  [frxDBMain."Nar5"]')
          SuppressRepeated = True
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object frxDBMainQty: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 419.527830000000000000
          Top = 23.677143390000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Qty'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Qty"]')
        end
        object frxDBMainRate: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 510.236550000000000000
          Top = 23.677143385826800000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Rate'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Rate"]')
        end
        object frxDBMainAmount: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 608.504330000000000000
          Top = 23.677143385826800000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'Amount'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Amount"]')
        end
      end
    end
  end
  object frxDBMain: TfrxDBDataset
    UserName = 'frxDBMain'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TransID=TransID'
      'BillNo=BillNo'
      'BillDate=BillDate'
      'ItemID=ItemID'
      'ItemName=ItemName'
      'Qty=Qty'
      'Rate=Rate'
      'Amount=Amount'
      'Nar1=Nar1'
      'Nar2=Nar2'
      'Nar3=Nar3'
      'Nar4=Nar4'
      'Nar5=Nar5')
    DataSet = QryMain
    BCDToCurrency = False
    Left = 128
    Top = 128
  end
  object frxReport2: TfrxReport
    Version = '6.4'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick, pbCopy, pbSelection]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 44459.658588449100000000
    ReportOptions.LastChange = 44459.674140219900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.          ')
    Left = 288
    Top = 120
    Datasets = <
      item
        DataSet = frxDBMain
        DataSetName = 'frxDBMain'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page2: TfrxReportPage
      PaperWidth = 297.000000000000000000
      PaperHeight = 420.000000000000000000
      PaperSize = 8
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
    end
    object Page1: TfrxReportPage
      PaperWidth = 297.000000000000000000
      PaperHeight = 420.000000000000000000
      PaperSize = 8
      Color = clMoneyGreen
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 1122.520410000000000000
        DataSet = frxDBMain
        DataSetName = 'frxDBMain'
        RowCount = 1
        Stretched = True
        object frxDBMainBillNo: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 1.000000000000000000
          Width = 158.740260000000000000
          Height = 18.897650000000000000
          DataField = 'BillNo'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."BillNo"]')
          ParentFont = False
        end
        object frxDBMainBillDate: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 1.000000000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataField = 'BillDate'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."BillDate"]')
          ParentFont = False
        end
        object frxDBMainItemName: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 23.677143390000000000
          Width = 400.630180000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."ItemName"]'
            
              '[frxDBMain."Nar1"] [frxDBMain."Nar2"] [frxDBMain."Nar3"] [frxDBM' +
              'ain."Nar4"]  [frxDBMain."Nar5"]')
          ParentFont = False
          SuppressRepeated = True
          Formats = <
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end
            item
            end>
        end
        object frxDBMainQty: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 411.968770000000000000
          Top = 23.677143390000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Qty'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Qty"]')
          ParentFont = False
        end
        object frxDBMainRate: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 23.677143390000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DataField = 'Rate'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Rate"]')
          ParentFont = False
        end
        object frxDBMainAmount: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 23.677143390000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DataField = 'Amount'
          DataSet = frxDBMain
          DataSetName = 'frxDBMain'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -17
          Font.Name = 'calibry'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDBMain."Amount"]')
          ParentFont = False
        end
      end
    end
  end
end
